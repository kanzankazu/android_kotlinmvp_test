package com.kanzankazu.kotlintest.presenter

import android.app.Activity
import android.content.Context
import com.kanzankazu.kotlintest.contract.AddOrEditContract
import com.kanzankazu.kotlintest.model.Tasks
import com.kanzankazu.kotlintest.utils.DatabaseHandler
import com.kanzankazu.kotlintest.utils.Util

class AddOrEditPresenter : AddOrEditContract.presenter {

    private var context: Context
    private var view: AddOrEditContract.view

    private var db: DatabaseHandler

    constructor(context: Context, view: AddOrEditContract.view) {
        this.context = context
        this.view = view
        this.db = DatabaseHandler(context)
    }

    override fun saveNewTask(tasks: Tasks) {
        val success = db.addTask(tasks)
        if (success) {
            view.isfinish()
            Util.showToast(context as Activity, "Simpan Berhasil")
        } else {
            Util.showToast(context as Activity, "Simpan Gagal")
        }
    }

    override fun updateTask(tasks: Tasks) {
        val success = db.updateTask(tasks)
        if (success) {
            view.isfinish()
            Util.showToast(context as Activity, "Update Berhasil")
        } else {
            Util.showToast(context as Activity, "Update Gagal")
        }
    }

    override fun getTask(id: Int): Tasks {
        return db.getTask(id)
    }

    override fun deleteTask(id: Int): Boolean {
        return db.deleteTask(id)
    }

}