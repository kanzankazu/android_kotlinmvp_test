package com.kanzankazu.kotlintest.presenter

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.kanzankazu.kotlintest.contract.MainContract
import com.kanzankazu.kotlintest.model.Tasks
import com.kanzankazu.kotlintest.utils.DatabaseHandler
import com.kanzankazu.kotlintest.view.AddOrEditActivity

class MainPresenter : MainContract.presenter {

    private var context: Context
    private var view: MainContract.view

    private var db: DatabaseHandler

    constructor(context: Context, view: MainContract.view) {
        this.context = context
        this.view = view
        this.db = DatabaseHandler(context)
    }

    override fun moveAddTaskActivity(context: Activity) {
        val i = Intent(context, AddOrEditActivity::class.java)
        i.putExtra("Mode", "A")
        context.startActivity(i)
    }

    override fun moveEditTaskActivity(context: Activity, tasks: Tasks) {
        val i = Intent(context, AddOrEditActivity::class.java)
        i.putExtra("Mode", "E")
        i.putExtra("Id", tasks.id)
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context.startActivity(i)
    }

    override fun getAllData(): List<Tasks> {
        return db.task()
    }

    override fun getData(id: Int): Tasks {
        return db.getTask(id)
    }

    override fun deleteAllTask() {
        val isSucces = db.deleteAllTasks()
        if (isSucces) view.initRefreshAdapter()
    }

    override fun onRefresh() {
        view.initRefreshAdapter()
        if (view.isRefresh()) view.refreshDismiss()
    }
}