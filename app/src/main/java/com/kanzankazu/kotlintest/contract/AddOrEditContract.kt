package com.kanzankazu.kotlintest.contract

import com.kanzankazu.kotlintest.model.Tasks

interface AddOrEditContract {
    interface view {

        fun isfinish()
    }

    interface presenter {
        fun getTask(id: Int): Tasks
        fun saveNewTask(tasks: Tasks)
        fun updateTask(tasks: Tasks)

        fun deleteTask(id: Int): Boolean
    }

    interface interactor {

    }
}