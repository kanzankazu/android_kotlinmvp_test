package com.kanzankazu.kotlintest.contract

import android.app.Activity
import android.content.Context
import com.kanzankazu.kotlintest.model.Tasks

interface MainContract {
    interface view {
        fun initRefreshAdapter()

        fun isRefresh(): Boolean
        fun refreshDismiss()
    }

    interface presenter {
        fun onRefresh()

        fun getData(id: Int): Tasks
        fun getAllData(): List<Tasks>
        fun deleteAllTask()

        fun moveEditTaskActivity(context: Activity, tasks: Tasks)
        fun moveAddTaskActivity(context: Activity)
    }

    interface interactor {

    }
}