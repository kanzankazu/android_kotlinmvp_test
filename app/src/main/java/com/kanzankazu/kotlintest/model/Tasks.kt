package com.kanzankazu.kotlintest.model

class Tasks {
    var id: Int = 0
    var name: String = ""
    var desc: String = ""
    var completed: String = "N"

    constructor()

    constructor(id: Int, name: String, desc: String, completed: String) {
        this.id = id
        this.name = name
        this.desc = desc
        this.completed = completed
    }

    constructor(name: String, desc: String, completed: String) {
        this.name = name
        this.desc = desc
        this.completed = completed
    }


}
