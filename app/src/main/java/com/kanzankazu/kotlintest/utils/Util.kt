package com.kanzankazu.kotlintest.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.support.design.widget.Snackbar
import android.widget.Toast

object Util {
    fun showSnackBar(context: Activity, message: String) {
        Snackbar.make(context.getWindow().getDecorView(), message, Snackbar.LENGTH_SHORT).show()
    }

    @SuppressLint("ShowToast")
    fun showToast(context: Activity, message: String){
        Toast.makeText(context,message,Toast.LENGTH_SHORT)
    }

}