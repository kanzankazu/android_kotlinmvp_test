package com.kanzankazu.kotlintest.view

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import com.kanzankazu.kotlintest.R
import com.kanzankazu.kotlintest.adapter.TaskRecyclerAdapter
import com.kanzankazu.kotlintest.contract.MainContract
import com.kanzankazu.kotlintest.model.Tasks
import com.kanzankazu.kotlintest.presenter.MainPresenter
import com.kanzankazu.kotlintest.utils.Util
import kotlinx.android.synthetic.main.activity_main_conte.*
import kotlinx.android.synthetic.main.activity_main_coord.*

class MainActivity : AppCompatActivity(), MainContract.view {

    private lateinit var taskRecyclerAdapter: TaskRecyclerAdapter
    private lateinit var mainPresenter: MainPresenter

    private var listTasks: List<Tasks> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_coord)

        mainPresenter = MainPresenter(this, this)

        initRefreshAdapter()
        initListener()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.action_delete) {
            val dialog = AlertDialog.Builder(this)
                .setTitle("Info")
                .setMessage("Click 'YES' Delete All Tasks")
                .setPositiveButton("YES") { dialog, _ ->
                    mainPresenter.deleteAllTask()
                    Util.showToast(this, "Berhasil Di hapus Semua")
                    Util.showSnackBar(this, "Berhasil Di hapus Semua")
                    initRefreshAdapter()
                    dialog.dismiss()
                }
                .setNegativeButton("NO") { dialog, _ ->
                    dialog.dismiss()
                }
            dialog.show()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        initRefreshAdapter()
    }

    private fun initListener() {
        srMain.setOnRefreshListener {
            mainPresenter.onRefresh()
        }
        fabMain.setOnClickListener {
            mainPresenter.moveAddTaskActivity(this)
        }
    }

    override fun initRefreshAdapter() {
        listTasks = mainPresenter.getAllData()
        taskRecyclerAdapter = TaskRecyclerAdapter(listTasks, this, mainPresenter)
        rvMain.adapter = taskRecyclerAdapter
        rvMain.layoutManager = LinearLayoutManager(applicationContext)
    }

    override fun isRefresh(): Boolean {
        return srMain.isRefreshing
    }

    override fun refreshDismiss() {
    srMain.isRefreshing = false
    }
}
