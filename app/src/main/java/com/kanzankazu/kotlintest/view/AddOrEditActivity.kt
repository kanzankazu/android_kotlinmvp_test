package com.kanzankazu.kotlintest.view

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import com.kanzankazu.kotlintest.R
import com.kanzankazu.kotlintest.contract.AddOrEditContract
import com.kanzankazu.kotlintest.model.Tasks
import com.kanzankazu.kotlintest.presenter.AddOrEditPresenter
import com.kanzankazu.kotlintest.utils.Util
import kotlinx.android.synthetic.main.activity_add_edit.*

class AddOrEditActivity : AppCompatActivity(), AddOrEditContract.view {

    private lateinit var addEditPresenter: AddOrEditPresenter

    private var isEditMode = false
    private var id: Int = 0

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_edit)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        addEditPresenter = AddOrEditPresenter(this, this)

        initBundle()
        initContent()
        initListener()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    private fun initBundle() {
        if (intent != null && intent.getStringExtra("Mode") == "E") {
            isEditMode = true
            id = intent.getIntExtra("Id", 0)
            val tasks: Tasks = addEditPresenter.getTask(id)
            tvAddEditTName.setText(tasks.name)
            tvAddEditTDesc.setText(tasks.desc)
            scAddEditComplete.isChecked = tasks.completed == "Y"
            bAddEditSave.setText("Update")
            showHideBtnDelete(true)
        } else {
            isEditMode = false
            bAddEditSave.setText("Save")
            showHideBtnDelete(false)
        }
    }

    private fun initContent() {
    }

    private fun initListener() {
        bAddEditSave.setOnClickListener {
            val name = tvAddEditTName.text.toString()
            val desc = tvAddEditTDesc.text.toString()
            val isComplete: String = if (scAddEditComplete.isChecked) "Y" else "N"

            if (!isEditMode) {
                addEditPresenter.saveNewTask(Tasks(name, desc, isComplete))
            } else {
                addEditPresenter.updateTask(Tasks(id, name, desc, isComplete))
            }
        }
        bAddEditDelete.setOnClickListener {
            val dialog = AlertDialog.Builder(this)
                .setTitle("Info")
                .setMessage("Click 'YES' Delete the Task.")
                .setPositiveButton("YES") { dialog, _ ->
                    val success = addEditPresenter.deleteTask(id)
                    if (success) {
                        finish()
                        Util.showToast(this, "Berhasil Dihapus")
                    } else {
                        Util.showToast(this, "Gagal Terhapus")
                    }
                    dialog.dismiss()
                }
                .setNegativeButton("NO") { dialog, _ ->
                    dialog.dismiss()
                }
            dialog.show()
        }
    }

    private fun showHideBtnDelete(isVisible: Boolean) {
        bAddEditDelete.visibility = if (isVisible) View.VISIBLE else View.INVISIBLE
    }

    override fun isfinish() {
        onBackPressed()
    }
}
