package com.kanzankazu.kotlintest.adapter

import android.app.Activity
import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.kanzankazu.kotlintest.R
import com.kanzankazu.kotlintest.model.Tasks
import com.kanzankazu.kotlintest.presenter.MainPresenter


class TaskRecyclerAdapter : RecyclerView.Adapter<TaskRecyclerAdapter.TaskViewHolder> {

    var context: Context
    var mainPresenter: MainPresenter
    var models: ArrayList<Tasks>

    constructor(tasksList: List<Tasks>, context: Context, mainPresenter: MainPresenter) : super() {
        this.context = context
        this.mainPresenter = mainPresenter
        this.models = tasksList as ArrayList<Tasks>
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_main_list, parent, false)
        return TaskViewHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        val tasks = models[position]
        holder.tvName.text = tasks.name
        holder.tvDesc.text = tasks.desc
        if (tasks.completed == "Y")
            holder.llListMain.background = ContextCompat.getDrawable(
                context,
                R.color.colorSuccess
            )
        else
            holder.llListMain.background = ContextCompat.getDrawable(
                context,
                R.color.colorUnSuccess
            )

        holder.itemView.setOnClickListener {
            mainPresenter.moveEditTaskActivity(context as Activity, tasks)
        }
    }

    override fun getItemCount(): Int {
        return models.size
    }

    inner class TaskViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvName: TextView = view.findViewById(R.id.tvListMainName) as TextView
        var tvDesc: TextView = view.findViewById(R.id.tvListMainDesc) as TextView
        var llListMain: LinearLayout = view.findViewById(R.id.llListMain) as LinearLayout
    }

    fun setData(datas: List<Tasks>) {
        if (models.size > 0) {
            models.clear()
            models = datas as ArrayList<Tasks>
        } else {
            models = datas as ArrayList<Tasks>
        }
        notifyDataSetChanged()
    }

    fun replaceData(datas: List<Tasks>) {
        models.clear()
        models.addAll(datas)
        notifyDataSetChanged()
    }

    fun addDatas(datas: List<Tasks>) {
        models.addAll(datas)
        notifyItemRangeInserted(models.size, datas.size)
    }

    fun addData(data: Tasks) {
        models.add(data)
        notifyDataSetChanged()
    }

    fun addDataFirst(data: Tasks) {
        val position = 0
        models.add(position, data)
        notifyItemInserted(position)
    }

    fun removeAt(position: Int) {
        models.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, models.size)
    }

    fun removeDataFirst() {
        val position = 0
        models.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, models.size)
    }

    fun updateSingleData(data: Tasks, position: Int) {
        models.set(position, data)
        notifyItemChanged(position)
    }

}
